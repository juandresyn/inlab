var express = require('express');
var nunjucks = require('nunjucks');

var app = express();

// Setup nunjucks templating engine
nunjucks.configure('views', {
  autoescape: true,
  express: app
});

app.use('/static', express.static(__dirname + '/assets'));

app.set('port', process.env.PORT || 3000);

// Home page
app.get('/', function(req, res) {
  res.render('index.html');
});

// Event Single edit
app.get('/edit/(:id)', function(req, res) {
  res.render('event-form.html', {
    eventsType: [
      {
        name: 'Pass It On',
        value: 'pass-it-on'
      },
      {
        name: 'Connect IT',
        value: 'connect-it'
      },
      {
        name: 'Practices',
        value: 'practices'
      },
      {
        name: 'Kickoff',
        value: 'kickoff'
      },
    ],
    eventId: req.params.id,
    eventName: 'Front for Non-Fronts',
    eventDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ac venenatis justo, at ornare leo. Integer pulvinar vestibulum lectus, in ultricies elit tristique auctor. Sed pulvinar erat nec sagittis consectetur.',
    eventExhibitors: [
      {
        name: 'Juandres Yepes'
      },
      {
        name: 'Jhon Doe'
      }
    ],
    eventDate: '09/26/2017 2:00 PM - 09/26/2017 3:30 PM',
    eventType: 'kickoff',
    eventImages: {
      eventMain: 'https://d2xdl4ekgh4j79.cloudfront.net/courses/get-stunning-pr-profile-photo/l3.png',
      eventCover: 'http://www.f-covers.com/cover/minimalistic-hipster-owl-facebook-cover-timeline-banner-for-fb.jpg'
    }
  });
});

// Event Single view
app.get('/event/(:id)', function(req, res) {
  res.render('event.html', {
    eventId: req.params.id,
    eventName: 'Front for Non-Fronts',
    eventDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ac venenatis justo, at ornare leo. Integer pulvinar vestibulum lectus, in ultricies elit tristique auctor. Sed pulvinar erat nec sagittis consectetur.',
    eventExhibitors: [
      {
        name: 'Juandres Yepes'
      },
      {
        name: 'Jhon Doe'
      }
    ],
    eventDate: '09/26/2017 2:00 PM - 09/26/2017 3:30 PM',
    eventType: 'kickoff',
    eventImages: {
      eventMain: 'https://d2xdl4ekgh4j79.cloudfront.net/courses/get-stunning-pr-profile-photo/l3.png',
      eventCover: 'http://www.f-covers.com/cover/minimalistic-hipster-owl-facebook-cover-timeline-banner-for-fb.jpg'
    }
  });
});

// Event new
app.get('/new', function(req, res) {
  res.render('event-form.html', {
    eventsType: [
      {
        name: 'Pass It On',
        value: 'pass-it-on'
      },
      {
        name: 'Connect IT',
        value: 'connect-it'
      },
      {
        name: 'Practices',
        value: 'practices'
      },
      {
        name: 'Kickoff',
        value: 'kickoff'
      },
    ]
  });
});

// Lock page
app.get('/lock', function(req, res) {
  res.render('lockscreen-3.html');
});

// Login page
app.get('/login', function(req, res) {
  res.render('login-1.html');
});

// Kick start our server
app.listen(app.get('port'), function() {
  console.log('Server started on port', app.get('port'));
});
